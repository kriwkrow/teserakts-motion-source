# Teseraksts Motion Source

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)

Uz doto brīdi aplikācija sūta parametrus 12 zonām.

Palaižot aplikāciju, tā sāk sūtīt OSC signālus uz broadcast IP adresi, kas tiek definēta `bin/data/settings.json` failā un portu `12345`. MaxMSP patch pusē ir jāatver OSC ports `54321`, uz to tiks sūtīti dati sekojošā formā.

```
addr: /teserakts/zone/0/dist: 0.543
addr: /teserakts/zone/0/dens: 0.334
addr: /teserakts/zone/1/dist: 0.444
addr: /teserakts/zone/1/dens: 0.234
...
addr: /teserakts/zone/2/dist: 0.234
addr: /teserakts/zone/2/dens: 0.467
```

Katrai OSC ziņai ir adrese `/teserakts/zone/0/dist` un `/teserakts/zone/0/dens`, kur pēdējā daļa ir zonas numurs sākot no `0` līdz `11` (kopā `12` zonas). Aiz zonas ID nāk parametra nosaukums: `dist` (distance) un `dens` (density, blīvums).

- `dist` ir attālums no realsense kameras robežās no `0.0` līdz `1.0`. 
- `dens` ir attiecīgās zonas blīvums, cik daudz cilvēku tur atrodas, arī skalā no `0.0` līdz `1.0`.

## Konfigurācija

Galvenais config fails ir `bin/data/settings.json`. `"cam_mode":1` tiek lietots, ja RealSense kameras ir pieslēgtas. `"cam_mode":0` tiek lietots, ja kameras nav pieslēgtas, bet nepieciešami cipari, kas mainās. Tas ir sākotnējais risinājums ar oscilatoriem.

Lai pa virsu nefloatotu error ziņas un visādi sveicieni, kad lietotne iestartējas, nospied "f", klikšķini ar labo peles pogu uz loga augšējās vadības joslas un izvēlies "Always on Top" opciju.

## Automātiska palaišana

Automātisku palaišanu un uzturēšanu pie dzīvības (keepalive) nodrošina systemd serviss (`systemd/teserakts.service`).

Ja tas nav iedarbināts, to jādara sekojoši.

```
sudo cp systemd/teserakts.service /etc/systemd/system
sudo systemctl enable teserakts.service
sudo systemctl start teserakts.service
```

Teserakts tiks palaists fullscreen režīmā. Lai no tā izietu, spied `f` un nospied krustiņu. Teserakts atkal atvērsies pēc 30 sekundēm. Ja to negribās, jādara sekojošais.

```
sudo systemctl stop teserakts.service
```

Ar šo Teserakta serviss tiks apturēts un būs iespējams netraucēti veikt citas datordarbības. Pēc pārstartēšanas serviss iestartēsies pats atkal.

## IP Addresses

Use broadcast IP address and same port for all computers.

**Broadcast**
Host: 192.168.88.255
Port: 54321

**Video PC**
Host: 192.168.88.254

**Audio PC**
Host: 192.168.88.251

## Troubleshooting

Gadās tā, ka nekas nestrādā. Realsense kameras ir diezgan jūtīgs pasākums. Risinājumi ir sekojoši.

### 1. Plug-Out Plug-In

Izraut USB vadus pa vienam un iespraust tos atpakaļ. Pārstartēt aplikāciju.

### 2. Kameru Hardware Reset

Atver termināli ar Ctrl + Alt + T. Palaid `realsense-viewer`. Ja kameras izmanto USB 2.1 protokolu, katrai atsevišķi vajag uztaisīt hardware reset. Šī opcija ir pieejama caur hamburger menu katras kameras paneļa labajā augšējā stūrī.

