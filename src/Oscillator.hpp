//
//  Oscillator.hpp
//  teseraktsMotionSource
//
//  Created by Krisjanis Rijnieks on 11/10/2019.
//

#pragma once

#include "ofMain.h"

namespace ta {

class Oscillator {
public:
	Oscillator();
	void setTimeMultiplier(float m);
	void setOffsetTime(float t);
	void update();
	float getValue();
	
private:
	float timeMultiplier;
	float startTime;
	float currentTime;
	float offsetTime;
	float value;
};

} // namespace ta

