#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxJSON.h"
#include "Oscillator.hpp"

#include <librealsense2/rs.hpp>

#define TA_MOTION_SOURCE_APP_TITLE "Teserakts Motion Source"

#ifdef TARGET_OSX
	#define TA_SETTINGS_FILE "settings.osx.json"
#else
	#define TA_SETTINGS_FILE "settings.json"
#endif

#define TA_MOTION_SOURCE_APP_FPS 30
#define TA_MOTION_SOURCE_CAM_FPS 30
#define TMS_CAM_WIDTH 640
#define TMS_CAM_HEIGHT 360

struct RSCamera{
	string serial;
	rs2::pipeline pipe;
};

struct OSCClient{
	string host;
	int port;
};

struct Zone{
	float dist;
	float dens;
};

class ofApp : public ofBaseApp{
public:
	void setup();
	void update();
	void draw();
	void exit();
	void keyPressed(int key);
	
	float distanceMin;
	float distanceMax;
	
	float densityMin;
	float densityMax;
	
	int camWidth;
	int camHeight;
	
	glm::vec3 pointcloud_scale;
	
	std::string appTitle;
	
	std::vector<ta::Oscillator> oscillators;

	rs2::context ctx;
	std::vector<rs2::depth_frame> depth_frames;
	
	std::vector<float> densities;
	std::vector<float> distances;
	
	ofxJSONElement settings;
	std::vector<RSCamera> cameras;
	std::vector<ofFbo> pcFbos;
	ofVboMesh mesh;
	
	std::vector<rs2::points> points;
    std::vector<rs2::pointcloud> pointclouds;
	
    ofEasyCam cam;
	
	// To send to broadcast address
	OSCClient oscClient;
	ofxOscSender oscSender;
	
	std::vector<Zone> zones;
	
	bool cam_mode;
	unsigned int num_zones;
	float interpolation_pct;
	float pointcloudPctY;
	
	static bool fullscreen;
	
	ofImage infoImage; // this for the bg
	ofImage authorsImage; // this for sliding text
	glm::vec2 authorsPos;
	float authorsSpeed;
};
