//
//  Oscillator.cpp
//  teseraktsMotionSource
//
//  Created by Krisjanis Rijnieks on 11/10/2019.
//

#include "Oscillator.hpp"

namespace ta {

Oscillator::Oscillator(){
	timeMultiplier = 0.5f;
	startTime = ofGetElapsedTimef();
	currentTime = startTime;
}

void Oscillator::setTimeMultiplier(float m){
	timeMultiplier = m;
}

void Oscillator::setOffsetTime(float t){
	offsetTime = t;
}

void Oscillator::update(){
	currentTime = ofGetElapsedTimef();
	value = (sin((currentTime - startTime + offsetTime) * timeMultiplier) + 1.0f) * 0.5f;
}

float Oscillator::getValue(){
	return value;
}

} // namespace ta
