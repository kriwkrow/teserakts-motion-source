#include "ofApp.h"

bool ofApp::fullscreen = false;

void ofApp::setup(){
	ofBackground(0);
	ofSetFrameRate(TA_MOTION_SOURCE_APP_FPS);
	
	appTitle = TA_MOTION_SOURCE_APP_TITLE;
	ofSetWindowTitle(appTitle);
	
	camWidth = TMS_CAM_WIDTH;
	camHeight = TMS_CAM_HEIGHT;

	ofLog() << "Loading settings...";
	
	std::string file = TA_SETTINGS_FILE;
	bool parseSuccess = settings.open(file);
	
	if(!parseSuccess){
		ofLogError() << "Failed to parse " << TA_SETTINGS_FILE;
		std::exit(EXIT_FAILURE);
	}
	
	ofLog() << "Settings loaded, setting up...";
	ofLog() << "Num OSC clients: " << settings["osc"]["clients"].size();
	ofLog() << "Num RealSense cameras: " << settings["realsense"]["cameras"].size();
	
	distanceMin = settings["realsense"]["distance"]["min"].asFloat();
	distanceMax = settings["realsense"]["distance"]["max"].asFloat();
	
	ofLog() << "Distance min: " << distanceMin;
	ofLog() << "Distance max: " << distanceMax;
	
	densityMin = settings["realsense"]["density"]["min"].asFloat();
	densityMax = settings["realsense"]["density"]["max"].asFloat();
	
	ofLog() << "Density min: " << densityMin;
	ofLog() << "Density max: " << densityMax;
	
	cam_mode = settings["cam_mode"].asBool();
	num_zones = settings["num_zones"].asUInt();
	interpolation_pct = settings["interpolation_pct"].asFloat();
	pointcloud_scale.x = settings["pointcloud"]["scale"]["x"].asFloat();
	pointcloud_scale.y = settings["pointcloud"]["scale"]["y"].asFloat();
	pointcloud_scale.z = settings["pointcloud"]["scale"]["z"].asFloat();
	
	// Configure OSC client
	oscClient.host = settings["osc"]["client"]["host"].asString();
	oscClient.port = settings["osc"]["client"]["port"].asInt();
	oscSender.setup(oscClient.host, oscClient.port);

	// Setup zones array so we don't have to clear it allways
	zones.resize(num_zones);

	if(cam_mode){
		
		// Prepare RealSense cameras ordered list
		cameras.resize(settings["realsense"]["cameras"].size());
		for(unsigned int i = 0; i < cameras.size(); i++){
			cameras[i].serial = settings["realsense"]["cameras"][i]["serial"].asString();
		}
		
		// Create a mesh for each camera
		// and a pointcloud
		// and an array of point containers
		pointclouds.resize(cameras.size());
		points.resize(cameras.size());
		
		pcFbos.resize(cameras.size());
		for(ofFbo & fbo : pcFbos){
			fbo.allocate(camWidth, camHeight);
			fbo.begin();
			ofClear(0, 0, 0, 0);
			fbo.end();
		}
	}
	
	ofxOscMessage m;
	m.setAddress("/teserakts/status");
	m.addStringArg("ONLINE");
	oscSender.sendMessage(m);
	
	ofLog() << "Done!";
	
	if(!cam_mode){
	
		// We have two oscillators per zone, that is why * 2
		oscillators.resize(num_zones * 2);
		for(auto & o : oscillators){
			o.setOffsetTime(ofRandom(10.0f));
			o.setTimeMultiplier(ofRandom(1.0f));
		}
	} else {
	
		// Start a streaming pipe per each connected Realsense device
		for (auto&& dev : ctx.query_devices()){
		
			// We want to make sure that each camera detected
			// is positioned according to the order found in settings.
			int index = -1;
		
			// We iterate over cameras from settings and compare
			// serials there with the current serial of the loop.
			// If there is a match, it means the position of the
			// camera equals current loop iteration index.
			for(unsigned int i = 0; i < cameras.size(); i++){
				std::string serial = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
				ofLog() << "Compare serial from settings " << cameras[i].serial << " vs camera " << serial;
				if(serial == cameras[i].serial){
					ofLog() << "Match! Index = " << i;
					index = i;
					break;
				}
			}
		
			// If there is no match found, we continue looping over devices.
			if(index < 0){
				continue;
			}
		
			// Create camera pipeline.
			rs2::pipeline pipe(ctx);
    		rs2::config cfg;
			cfg.enable_device(dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER));
    		cfg.enable_stream(RS2_STREAM_DEPTH, -1, camWidth, camHeight, RS2_FORMAT_ANY, TA_MOTION_SOURCE_CAM_FPS);
    		pipe.start(cfg);
		
			cameras[index].pipe = pipe;
		}
	}
	
	infoImage.load("infotext.jpg");
	authorsImage.load("authors.jpg");
	authorsPos = glm::vec2(0.0f, 0.66f); // pct
	authorsSpeed = 0.1f; // pct per second
	pointcloudPctY = 0.71f;
	
	if(fullscreen){
		ofSetFullscreen(true);
		ofHideCursor();
	}
	
	cam.enableOrtho();
	cam.setFarClip(-1000.0f);
	cam.setNearClip(1000.0f);
	cam.setPosition(0, 0, 1000.0f);
	cam.lookAt(glm::vec3(0, 0, 0));
}

void ofApp::update(){
	if(!cam_mode){
		
		// Update oscillators
		for(auto & o : oscillators){
			o.update();
		}
		
		for(unsigned int i = 0; i < oscillators.size(); i += 2){
			
			// We go two steps each iteration
			// to add two values per zone.
			Zone z;
			z.dist = oscillators[i].getValue();
			z.dens = oscillators[i + 1].getValue();
			zones[i] = z;
		}
		
	} else {
	
		// Collect the new frames from all the connected devices
		mesh.clear();
		depth_frames.clear();
		
		for(unsigned int i = 0; i < cameras.size(); i++){
			RSCamera & c = cameras[i];
   			auto frames = c.pipe.wait_for_frames();
   			auto depth = frames.get_depth_frame();
			
			depth_frames.emplace_back(depth);
			
			// Construct pointcoulds as of meshes
			points[i] = pointclouds[i].calculate(depth);
			
			unsigned int numPoints = points[i].size();
			
			if(numPoints == 0){
				continue;
			}
			
			const rs2::vertex * vs = points[i].get_vertices();
			
			mesh.clear();
			
			for(unsigned int vi = 0; vi < numPoints; vi += cameras.size()){
				if(!vs[vi].z){
					continue;
				}
			
				const rs2::vertex v = vs[vi];
				glm::vec3 v3(v.x, v.y, v.z);
				mesh.addVertex(v3);
				
				float chan = ofMap(v.z, 0.0f, 7.5f, 0.25f, 1.0f);
				mesh.addColor(ofFloatColor(chan, chan, chan, 0.8f));
			}
			
			pcFbos[i].begin();
			ofClear(0, 0, 0, 0);
			
			cam.begin();
    		ofScale(
    			-pointcloud_scale.x,
    			-pointcloud_scale.y,
    			pointcloud_scale.z);
			mesh.drawVertices();
			cam.end();
			
			pcFbos[i].end();
		}
		
		densities.clear();
		distances.clear();
	
		for(unsigned int i = 0; i < depth_frames.size(); i++){
			rs2::depth_frame & df = depth_frames[i];
		
			// Go over each vertical pixel stride in the image.
			for(int x = camWidth-1; x >= 0; x--){
		
				// Look for minimum value, closest object.
				float min = 100.0f;
			
				// Add one if distance within min/max range found 0 otherwise.
				float in_range_count = 0.0;
			
				for(int y = 0; y < camHeight; y++){
					float d = df.get_distance(x, y);
				
					// This is for density calculation
					if(ofInRange(d, distanceMin, distanceMax)){
						in_range_count += 1.0f;
					}
			
					// This is for distance calculation
					if(d < min && d >= distanceMin){
						min = d;
					}
				}
				
				//ofLog() << "min " << x << ": " << min;

				// Calculate density for current column
				float density = in_range_count / (float)camHeight;
				density = ofClamp(density, densityMin, densityMax);
				densities.push_back(density);
			
				// Calculate distance of closest point
				min = ofClamp(min, distanceMin, distanceMax);
				distances.push_back(min);
			}
		}
		
		// Calculate how many pixel columns per zone
		int cols_per_zone = (int)((float)distances.size() / (float)num_zones);
	
		unsigned int zoneIndex = 0;
		
		for(unsigned int col = 0; col < distances.size(); col += cols_per_zone){
			float densities_sum = 0.0f;
			float min = 100.0f;
			
			for(unsigned int x = col; x < (unsigned int)(col + cols_per_zone); x++){
			
				// Get average density for zone
				densities_sum += densities[x];
			
				// Get min distance for the zone
				if(distances[x] < min){
					min = distances[x];
				}
			}
		
			// Add a zone,
			// normalize density and distance
			float avg_dens = densities_sum / (float)cols_per_zone;
		
			if(zoneIndex < zones.size()){
		
				// We convert min/max range to 0.0 - 1.0.
				float dens = (avg_dens - densityMin) / (densityMax - densityMin);
				zones[zoneIndex].dens = ofInterpolateCosine(zones[zoneIndex].dens, dens, interpolation_pct);
		
				// Normalize stored distance for zone and invert it.
				// 1.0 if object is close.
				// 0.0 if object is far away.
				float dist = 1.0f - ((min - distanceMin) / (distanceMax - distanceMin));
				zones[zoneIndex].dist = ofInterpolateCosine(zones[zoneIndex].dist, dist, interpolation_pct);
			}
			
			zoneIndex++;
		}
	}

	// Send values via OSC
	ofxOscBundle b;
	for(unsigned int i = 0; i < zones.size(); i++){
		ofxOscMessage dist;
		dist.setAddress("/teserakts/zone/" + ofToString(i) + "/dist");
		dist.addFloatArg(zones[i].dist);
		b.addMessage(dist);
		
		ofxOscMessage dens;
		dens.setAddress("/teserakts/zone/" + ofToString(i) + "/dens");
		dens.addFloatArg(zones[i].dens);
		b.addMessage(dens);
	}
	
	oscSender.sendBundle(b);
}

void ofApp::draw(){
	
	// Draw background
	infoImage.draw(0, 0, ofGetWidth(), ofGetHeight());
	
	// Draw authors sliding image
	glm::vec2 posPix = authorsPos * glm::vec2(ofGetWidth(), ofGetHeight());
	authorsImage.draw(posPix);
	authorsImage.draw(posPix.x + authorsImage.getWidth(), posPix.y);
	
	authorsPos.x -= authorsSpeed * ofGetLastFrameTime();
	if( (int)(authorsPos.x * (float)ofGetWidth()) < -authorsImage.getWidth() ){
		authorsPos.x = 0.0f;
	}
	
	// Draw zones
	int pix_per_zone = (int)((float)ofGetWidth() / (float)num_zones);
	for(unsigned int z = 0; z < zones.size(); z++){
		ofPushStyle();
		
		// Convert density to color brightness
		int col = (int)(zones[z].dens * 255.0f);
		ofSetColor(255, 255, 255, col);
		
		// Calculate bar height in pixels relative to window height.
		int barsPosY = (int)((float)ofGetHeight() * pointcloudPctY);
		int barsMaxHeight = ofGetHeight() - barsPosY;
		float hei = zones[z].dist * (float)barsMaxHeight;
		
		// If there are no objects in range, still show a bit of rect.
		if(hei <= 0.0f){
			hei = 10.0f;
		}
		
		ofDrawRectangle(
			z * pix_per_zone,
			ofGetHeight() - hei,
			pix_per_zone,
			hei);
		
		ofPopStyle();
		
		// Draw dist and dens values for each zone
		ofPushMatrix();
		
		ofTranslate(z * pix_per_zone, ofGetHeight());
		
		std::string zone_dist = "dist: " + ofToString(zones[z].dist, 2);
		ofDrawBitmapStringHighlight(zone_dist, glm::vec2(10.0f, -10.0f));
		
		std::string zone_dens = "dens: " + ofToString(zones[z].dens, 2);
		ofDrawBitmapStringHighlight(zone_dens, glm::vec2(10.0f, -25.0f));
		
		ofPopMatrix();
	}
	
	std::string screenMessage = appTitle + "\nZones: " + ofToString(num_zones) + "\nFPS: " + ofToString(ofGetFrameRate(), 2);
	ofDrawBitmapStringHighlight(screenMessage, 10, 20);
	
	// Draw pointcloud
	if(cam_mode){
		int pcPosY = (int)((float)ofGetHeight() * pointcloudPctY);
		int pcHeight = ofGetHeight() - pcPosY; // pc is for pointcloud
	
		int pcWidth = (int)((float)ofGetWidth() / (float)pcFbos.size());
		float ratio = (float)pcWidth / (float)camWidth;
		//int pcHeight = (int)((float)camHeight * ratio);
		
    	for(unsigned int i = 0; i < pcFbos.size(); i++){
			pcFbos[i].draw(i * pcWidth, pcPosY, pcWidth, pcHeight);
		}
    }
}

void ofApp::exit(){
	ofxOscMessage m;
	m.setAddress("/teserakts/status");
	m.addStringArg("OFFLINE");
	oscSender.sendMessage(m);
}

void ofApp::keyPressed(int key){
	if(key == 'f'){
		fullscreen = !fullscreen;
		ofSetFullscreen(fullscreen);
		
		if(fullscreen){
			ofHideCursor();
		} else {
			ofShowCursor();
		}
	}
}
