#include "ofMain.h"
#include "ofApp.h"

int main(int argc, char *argv[]){
	
	std::cout << "Use -f to launch fullscreen" << std::endl;
	
	if(argc >= 2){
		std::string flag(argv[1]);
		if(flag == "-f"){
			ofApp::fullscreen = true;
		}
	}
	
	ofSetupOpenGL(600, 600, OF_WINDOW);
	ofRunApp(new ofApp());
}
